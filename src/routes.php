<?php

use DB;

Route::get('comments', function() {
  $comments = DB::('comments')
              ->get();
  return view('comment::comments')->->with('comments', $comments);
});
