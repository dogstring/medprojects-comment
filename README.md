## Install

Via composer.json
``` bash
"medprojects/comment":"0.1.*"

```
Via terminal
``` bash
$ composer require medprojects/comment
```

## Usage

``` php
php artisan vendor:publish
php artisan migrate
```
